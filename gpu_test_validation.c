#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include <cublas_v2.h>
#include <cuComplex.h>
#include <cuda_runtime.h>
#pragma GCC diagnostic pop

#include <cufft.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define CUDACHECK( call ) {\
	cudaError_t error = call;\
	if( error != cudaSuccess ){\
		fprintf( stderr, "CUDA Error: %s (at %s:%d)\n", cudaGetErrorString(error), __FILE__, __LINE__);\
		exit(1);\
	}\
}

#define CUFFTCECK( call ) {\
	cufftResult error = call;\
	if( error != CUFFT_SUCCESS ){\
		fprintf( stderr, "CUFFT Error: %d (at %s:%d)\n", error, __FILE__, __LINE__);\
		exit(1);\
	}\
}

#define CUBLASCECK( call ) {\
	cublasStatus_t error = call;\
	if( error != CUBLAS_STATUS_SUCCESS ){\
		fprintf( stderr, "CUBLAS Error: %d (at %s:%d)\n", error, __FILE__, __LINE__);\
		exit(1);\
	}\
}

void poly_gemm_transpose( int m, int n, int k, int r, cuComplex* A, cuComplex* B, cuComplex* C );

cufftHandle _plan_A, _plan_B, _plan_C;
cublasHandle_t _cublas_handle;
cuComplex *_wA, *_wB, *_wC;

int main( int argc, char** argv )
{
    long long int m = 2;
    long long int n = 3;
    long long int k = 3;
    long long int r = 4;
    
    // Allocate
    cuComplex *A, *B, *C;
    CUDACHECK( cudaMallocManaged( (void**) &A, sizeof(cuComplex) * m * k * r, cudaMemAttachGlobal ) )  
    CUDACHECK( cudaMallocManaged( (void**) &B, sizeof(cuComplex) * k * n * r, cudaMemAttachGlobal ) )
    CUDACHECK( cudaMallocManaged( (void**) &C, sizeof(cuComplex) * m * n * r, cudaMemAttachGlobal ) )

    CUDACHECK( cudaMallocManaged( (void**) &_wA, sizeof(cuComplex) * m * k * r, cudaMemAttachGlobal ) )   
    CUDACHECK( cudaMallocManaged( (void**) &_wB, sizeof(cuComplex) * k * n * r, cudaMemAttachGlobal ) )
    CUDACHECK( cudaMallocManaged( (void**) &_wC, sizeof(cuComplex) * m * n * r, cudaMemAttachGlobal ) )         

    // Prepare FFT plans and cublas handle
    CUBLASCECK( cublasCreate( &_cublas_handle ) )
    CUFFTCECK( cufftPlan1d( &_plan_A, r, CUFFT_C2C, m * k) );
    CUFFTCECK( cufftPlan1d( &_plan_B, r, CUFFT_C2C, k * n) );
    CUFFTCECK( cufftPlan1d( &_plan_C, r, CUFFT_C2C, m * n) );

    // Fill data
    memset( A, 0, sizeof(cuComplex) * m * k * r );
    memset( B, 0, sizeof(cuComplex) * k * n * r );
    A[0 + 0].x = 1; A[0 + 1].x = 2; A[0 + 2].x = 3; A[0 + 3].x = 4;
    A[4 + 0].x = 5; A[4 + 1].x = 6; A[4 + 2].x = 7; A[4 + 3].x = 8;
    A[8 + 0].x = 9; A[8 + 1].x = 1; A[8 + 2].x = 2; A[8 + 3].x = 3;
    A[12 + 0].x = 8; A[12 + 1].x = 9; A[12 + 2].x = 1; A[12 + 3].x = 2;
    A[16 + 0].x = 3; A[16 + 1].x = 4; A[16 + 2].x = 5; A[16 + 3].x = 6;
    A[20 + 0].x = 7; A[20 + 1].x = 8; A[20 + 2].x = 9; A[20 + 3].x = 1;

    B[0 + 0].x = 1; B[0 + 1].x = 3; B[0 + 2].x = 8; B[0 + 3].x = 7;
    B[4 + 0].x = 1; B[4 + 1].x = 1; B[4 + 2].x = 3; B[4 + 3].x = 2;
    B[8 + 0].x = 1; B[8 + 1].x = 2; B[8 + 2].x = 1; B[8 + 3].x = 1;
    B[12 + 0].x = 2; B[12 + 1].x = 2; B[12 + 2].x = 3; B[12 + 3].x = 1;
    B[16 + 0].x = 1; B[16 + 1].x = 2; B[16 + 2].x = 9; B[16 + 3].x = 3;
    B[20 + 0].x = 0; B[20 + 1].x = 0; B[20 + 2].x = 0; B[20 + 3].x = 0;
    B[24 + 0].x = 1; B[24 + 1].x = 2; B[24 + 2].x = 5; B[24 + 3].x = 2;
    B[28 + 0].x = 4; B[28 + 1].x = 3; B[28 + 2].x = 5; B[28 + 3].x = 1;
    B[32 + 0].x = 1; B[32 + 1].x = 4; B[32 + 2].x = 5; B[32 + 3].x = 1;

		/*
    B[0 + 0].x = 1; B[0 + 1].x = 0; B[0 + 2].x = 0; B[0 + 3].x = 0;
    B[4 + 0].x = 0; B[4 + 1].x = 0; B[4 + 2].x = 0; B[4 + 3].x = 0;
    B[8 + 0].x = 0; B[8 + 1].x = 0; B[8 + 2].x = 0; B[8 + 3].x = 0;

    B[12 + 0].x = 1; B[12 + 1].x = 0; B[12 + 2].x = 0; B[12 + 3].x = 0;
    B[16 + 0].x = 1; B[16 + 1].x = 0; B[16 + 2].x = 0; B[16 + 3].x = 0;
    B[20 + 0].x = 0; B[20 + 1].x = 0; B[20 + 2].x = 0; B[20 + 3].x = 0;

    B[24 + 0].x = 0; B[24 + 1].x = 0; B[24 + 2].x = 0; B[24 + 3].x = 0;
    B[28 + 0].x = 0; B[28 + 1].x = 0; B[28 + 2].x = 0; B[28 + 3].x = 0;
    B[32 + 0].x = 1; B[32 + 1].x = 0; B[32 + 2].x = 0; B[32 + 3].x = 0;
		*/


    // Compute
    poly_gemm_transpose( m, n, k, r, A, B, C );
    CUDACHECK( cudaDeviceSynchronize() )

    for( int j = 0; j < m; j++ )
    {
    	for( int i = 0; i < n; i++ )
        {
            printf("[ ");
            for( int v = 0; v < r; v++ )
            {
                printf("%+.2f %+.2fi ", C[ v + r * ( j + m * i ) ].x, C[ v + r * ( j + m * i ) ].y);
            }
            printf("] ");
        }
        printf("\n");
    }


    // Clean
    CUBLASCECK( cublasDestroy( _cublas_handle ) )
    CUFFTCECK( cufftDestroy( _plan_A ) )
    CUFFTCECK( cufftDestroy( _plan_B ) )
    CUFFTCECK( cufftDestroy( _plan_C ) )

    CUDACHECK( cudaFree( A ) )
    CUDACHECK( cudaFree( B ) )    
    CUDACHECK( cudaFree( C ) )
    CUDACHECK( cudaFree( _wA ) )
    CUDACHECK( cudaFree( _wB ) )    
    CUDACHECK( cudaFree( _wC ) )

    return 0;
}

void poly_gemm_transpose( int m, int n, int k, int r, cuComplex* A, cuComplex* B, cuComplex* C )
{
        // Place A and B in "frequency domain"
    CUFFTCECK( cufftExecC2C( _plan_A, A, A, CUFFT_FORWARD ) );
    CUFFTCECK( cufftExecC2C( _plan_B, B, B, CUFFT_FORWARD ) );
    CUDACHECK( cudaDeviceSynchronize() );
    
    // "Transpose" A and B
    // From [r][m*k] -> [m*k][r]
    cuComplex alpha = {1,0};
    cuComplex beta = {0,0};
    CUBLASCECK( cublasCgeam( _cublas_handle, CUBLAS_OP_T, CUBLAS_OP_T, m*k, r, &alpha, A, r, &beta, A, r, _wA, m*k ) )
    CUBLASCECK( cublasCgeam( _cublas_handle, CUBLAS_OP_T, CUBLAS_OP_T, k*n, r, &alpha, B, r, &beta, B, r, _wB, k*n ) )

    // Apply the gemm for eaC freq
    // compute types: CUBLAS_COMPUTE_32F (cuda core) or CUBLAS_COMPUTE_32F_FAST_TF32 (tensorCore if TF32 supported) or CUBLAS_COMPUTE_32F_FAST_16F (tensorCore if FP16 supported)
    CUBLASCECK( cublasGemmStridedBatchedEx( _cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N,
                    m, n, k,
                    &alpha, 
                    (void*) _wA, CUDA_C_32F, m, (long long int) (m * k),
                    (void*) _wB, CUDA_C_32F, k, (long long int) (k * n),
                    &beta,
                    (void*) _wC, CUDA_C_32F, m, (long long int) (m * n),
                    r,
                    CUBLAS_COMPUTE_32F, CUBLAS_GEMM_DEFAULT ) )

    // "Transpose" C
    alpha.x = 1.0/r;
    CUBLASCECK( cublasCgeam( _cublas_handle, CUBLAS_OP_T, CUBLAS_OP_T, r, m*n, &alpha, _wC, m*n, &beta, _wC, m*n, C, r ) )

        // Place C in "temporal domain"
    CUFFTCECK( cufftExecC2C( _plan_C, C, C, CUFFT_INVERSE ) );

        // Reset A and B also:
    CUFFTCECK( cufftExecC2C( _plan_A, A, A, CUFFT_INVERSE ) );
    CUFFTCECK( cufftExecC2C( _plan_B, B, B, CUFFT_INVERSE ) );
}
