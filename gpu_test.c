/* Matrix multiplication between matrix of polynoms of size r (on GPU)
 * Operation C := A * B
 *
 * Idea: 
 *  Apply FFT on all polynoms of A and B                                 O( r * log(r) * n^2 )
 *    add: vector addition -> indep by rank
 *    mul: vector point to point multiplication -> indep by rank
 *  Reorder data to pack elements of the same 'rank'                     O( n^2 )
 *  Apply GEMM for each rank (use strideBatched)                         O( r * n^3 )
 *  Reorder output data (to pack polynoms togethers)                     O( n^2 )
 *  Apply IFFT to get final result                                       O( r * log(r) * n^2 )
 *
 *
 * The compexity is max( O( r * n^3 ), O( r * log(r) * n^2 ) )
 * The complexity if apply FFT at each mul is O( r * log(r) * n^3 ) ), it should be faster
 *
 * Warning: Values of A and B are moddified
 */

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include <cublas_v2.h>
#include <cuComplex.h>
#include <cuda_runtime.h>
#pragma GCC diagnostic pop

#include <cufft.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define CUDACHECK( call ) {\
	cudaError_t error = call;\
	if( error != cudaSuccess ){\
		fprintf( stderr, "CUDA Error: %s (at %s:%d)\n", cudaGetErrorString(error), __FILE__, __LINE__);\
		exit(1);\
	}\
}

#define CUFFTCHECK( call ) {\
	cufftResult error = call;\
	if( error != CUFFT_SUCCESS ){\
		fprintf( stderr, "CUFFT Error: %d (at %s:%d)\n", error, __FILE__, __LINE__);\
		exit(1);\
	}\
}

#define CUBLASCHECK( call ) {\
	cublasStatus_t error = call;\
	if( error != CUBLAS_STATUS_SUCCESS ){\
		fprintf( stderr, "CUBLAS Error: %d (at %s:%d)\n", error, __FILE__, __LINE__);\
		exit(1);\
	}\
}

void poly_gemm_transpose( int m, int n, int k, int r, cuComplex* A, cuComplex* B, cuComplex* C );

cufftHandle _plan_A, _plan_B, _plan_C;
cublasHandle_t _cublas_handle;
cuComplex *_wA, *_wB, *_wC;

int main( int argc, char** argv )
{
    long long int m = 0;
    long long int n = 0;
    long long int k = 0;
    long long int r = 0;
    long long int count = 1;
    long long int loop = 32;
    // Parse arguments
    for( int arg_id = 1; arg_id < argc; arg_id++ )
    {
        char* arg = argv[arg_id];
        
        if( strcmp( arg, "-h" ) == 0 )
        {
            // TODO implement help
        }
        else if( strcmp( arg, "-r" ) == 0 )
        {
            arg_id++;
            r = atoi( argv[arg_id] );
        }
        else if( strcmp( arg, "-m" ) == 0 )
        {
            arg_id++;
            m = atoi( argv[arg_id] );
        }
        else if( strcmp( arg, "-n" ) == 0 )
        {
            arg_id++;
            n = atoi( argv[arg_id] );
        }
        else if( strcmp( arg, "-k" ) == 0 )
        {
            arg_id++;
            k = atoi( argv[arg_id] );
        }
        else if( strcmp( arg, "-c" ) == 0 )
        {
            arg_id++;
            count = atoi( argv[arg_id] );
        }
        else if( strcmp( arg, "-l" ) == 0 )
        {
            arg_id++;
            loop = atoi( argv[arg_id] );
        }
    }
    
    // Allocate
    cuComplex *A, *B, *C;
    CUDACHECK( cudaMalloc( (void**) &A, sizeof(cuComplex) * m * k * r * count ) )    
    CUDACHECK( cudaMalloc( (void**) &B, sizeof(cuComplex) * k * n * r * count ) )
    CUDACHECK( cudaMalloc( (void**) &C, sizeof(cuComplex) * m * n * r * count ) )    

    CUDACHECK( cudaMalloc( (void**) &_wA, sizeof(cuComplex) * m * k * r ) )    
    CUDACHECK( cudaMalloc( (void**) &_wB, sizeof(cuComplex) * k * n * r ) )
    CUDACHECK( cudaMalloc( (void**) &_wC, sizeof(cuComplex) * m * n * r ) )     

    // Prepare FFT plans and cublas handle
    CUBLASCHECK( cublasCreate( &_cublas_handle ) )
    CUFFTCHECK( cufftPlan1d( &_plan_A, r, CUFFT_C2C, m * k) );
    CUFFTCHECK( cufftPlan1d( &_plan_B, r, CUFFT_C2C, k * n) );
    CUFFTCHECK( cufftPlan1d( &_plan_C, r, CUFFT_C2C, m * n) );
    
    // TODO load data to GPU


    // Warmup
    poly_gemm_transpose( m, n, k, r, A, B, C );
    
    struct timespec start, end;
    double elapsed_time;
    clock_gettime(CLOCK_MONOTONIC, &start);
    for( int id_loop = 0; id_loop < loop; id_loop++ )
    {
        for( int id_count = 0; id_count < count; id_count++ )
        {
            cuComplex* lA = A + m * k * r * id_count;
            cuComplex* lB = B + k * n * r * id_count;
            cuComplex* lC = C + m * n * r * id_count;

            poly_gemm_transpose( m, n, k, r, lA, lB, lC );
        }
    }
    CUDACHECK( cudaDeviceSynchronize() )
    clock_gettime(CLOCK_MONOTONIC, &end);
    elapsed_time = (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1.0e9;
    printf("[m=%4lld,n=%4lld,k=%4lld,r=%4lld,l=%lld,c=%lld] %f s -> %.0f poly%lld/s\n", m, n, k, r, loop, count, elapsed_time,
        (m * n * k * loop * count)/elapsed_time, r );

    // Clean
    CUBLASCHECK( cublasDestroy( _cublas_handle ) )
    CUFFTCHECK( cufftDestroy( _plan_A ) )
    CUFFTCHECK( cufftDestroy( _plan_B ) )
    CUFFTCHECK( cufftDestroy( _plan_C ) )

    CUDACHECK( cudaFree( A ) )
    CUDACHECK( cudaFree( B ) )    
    CUDACHECK( cudaFree( C ) )
    CUDACHECK( cudaFree( _wA ) )
    CUDACHECK( cudaFree( _wB ) )    
    CUDACHECK( cudaFree( _wC ) )

    return 0;
}

void poly_gemm_transpose( int m, int n, int k, int r, cuComplex* A, cuComplex* B, cuComplex* C )
{
        // Place A and B in "frequency domain"
    CUFFTCHECK( cufftExecC2C( _plan_A, A, A, CUFFT_FORWARD ) );
    CUFFTCHECK( cufftExecC2C( _plan_B, B, B, CUFFT_FORWARD ) );

    // "Transpose" A and B
    // From [r][m*k] -> [m*k][r]
    cuComplex alpha = {1,0};
    cuComplex beta = {0,0};
    CUBLASCHECK( cublasCgeam( _cublas_handle, CUBLAS_OP_T, CUBLAS_OP_T, m*k, r, &alpha, A, r, &beta, A, r, _wA, m*k ) )
    CUBLASCHECK( cublasCgeam( _cublas_handle, CUBLAS_OP_T, CUBLAS_OP_T, k*n, r, &alpha, B, r, &beta, B, r, _wB, k*n ) )

    // Apply the gemm for each freq
    // compute types: CUBLAS_COMPUTE_32F (cuda core) or CUBLAS_COMPUTE_32F_FAST_TF32 (tensorCore if TF32 supported) or CUBLAS_COMPUTE_32F_FAST_16F (tensorCore if FP16 supported)
    CUBLASCHECK( cublasGemmStridedBatchedEx( _cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N,
                    m, n, k,
                    &alpha, 
                    (void*) _wA, CUDA_C_32F, m, (long long int) (m * k),
                    (void*) _wB, CUDA_C_32F, k, (long long int) (k * n),
                    &beta,
                    (void*) _wC, CUDA_C_32F, m, (long long int) (m * n),
                    r,
                    CUBLAS_COMPUTE_32F, CUBLAS_GEMM_DEFAULT ) )

    // "Transpose" C
    alpha.x = 1.0/r; // Because CUFFT_INVERSE does not normalize
    CUBLASCHECK( cublasCgeam( _cublas_handle, CUBLAS_OP_T, CUBLAS_OP_T, r, m*n, &alpha, _wC, m*n, &beta, _wC, m*n, C, r ) )

        // Place C in "temporal domain"
    CUFFTCHECK( cufftExecC2C( _plan_C, C, C, CUFFT_INVERSE ) );

        // Maybe reset A and B (warning need to normalize)
    //CUFFTCHECK( cufftExecC2C( _plan_A, A, A, CUFFT_INVERSE ) );
    //CUFFTCHECK( cufftExecC2C( _plan_B, B, B, CUFFT_INVERSE ) );
}
