m = 2;
n = 3;
k = 3;
r = 4;

mC = zeros( [r,m,n] );
mB = zeros( [r,k,n] );
mA = zeros( [r,m,k] );

mA(:,1,1) = [1 2 3 4];
mA(:,2,1) = [5 6 7 8]; 

mA(:,1,2) = [9 1 2 3];
mA(:,2,2) = [8 9 1 2];

mA(:,1,3) = [3 4 5 6];
mA(:,2,3) = [7 8 9 1]; 

mB(:,1,1) = [1 3 8 7];
mB(:,2,1) = [1 1 3 2];
mB(:,3,1) = [1 2 1 1];

mB(:,1,2) = [2 2 3 1];
mB(:,2,2) = [1 2 9 3];
mB(:,3,2) = [0 0 0 0];

mB(:,1,3) = [1 2 5 2];
mB(:,2,3) = [4 3 5 1];
mB(:,3,3) = [1 4 5 1];

function C = pmul( A, B )
	fA = fft(A);
	fB = fft(B);
	fC = fA .* fB;
	C = ifft(fC);
end

for x = 1:m;
	for y = 1:n;
		for z = 1:k;
			mC(:,x,y) = mC(:,x,y) + pmul( mA(:,x,z), mB(:,z,y) );
		end;
	end;
end;

mC
