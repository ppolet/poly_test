#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

typedef struct float2 {
    float x;
    float y;
} float2;

void poly_add( const float2* A, const float2* B, float2* C, int r );
void poly_mul( const float2* A, const float2* B, float2* C, int r );
void poly_mul_fft( const float2* A, const float2* B, float2* C, int r );

// Cooley-tukey FFT
void fft( float2* X, int n );
void ifft( float2* X, int n );

int main( int argc, char** argv )
{
    int r = 32;
    int count = 256;
    int loop = 1024; 

    // Parse arguments
    for( int arg_id = 1; arg_id < argc; arg_id++ )
    {
        char* arg = argv[arg_id];
        
        if( strcmp( arg, "-h" ) == 0 )
        {
            // TODO implement help
        }
        else if( strcmp( arg, "-r" ) == 0 )
        {
            arg_id++;
            r = atoi( argv[arg_id] );
        }
        else if( strcmp( arg, "-c" ) == 0 )
        {
            arg_id++;
            count = atoi( argv[arg_id] );
        }
        else if( strcmp( arg, "-l" ) == 0 )
        {
            arg_id++;
            loop = atoi( argv[arg_id] );
        }
    }

    // Allocate
    float2* A = (float2*) malloc( sizeof(float2) * r * count );
    float2* B = (float2*) malloc( sizeof(float2) * r * count );
    float2* C = (float2*) malloc( sizeof(float2) * r * count );

    // Execution
    struct timespec start, end;
    double elapsed_time;
    clock_gettime(CLOCK_MONOTONIC, &start);
    for (int j = 0; j < loop; j++ )
    {
        for (int i = 0; i < count; i++) {
            OPERATION( A + r*i, B + r*i, C + r*i, r );
        }
    }
    clock_gettime(CLOCK_MONOTONIC, &end);

    // Time and print
    elapsed_time = (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1.0e9;
    printf("[T] %f s | %f poly/s\n", elapsed_time, (count*loop)/elapsed_time );

    // Clean
    free( A );
    free( B );
    free( C );

    return 0;
}

void poly_add( const float2* A, const float2* B, float2* C, int r )
{
    for( int i = 0; i < r; i++ )
    {
        float2 a = A[i];
        float2 b = B[i];
        float2 c;

        c.x = a.x + b.x;
        c.y = a.y + b.y;
        C[i] = c;
    }
}

void poly_mul( const float2* A, const float2* B, float2* C, int r )
{
    for( int i = 0; i < r; i++ )
    {
        float2 c;
        c.x = 0;
        c.y = 0;
        for( int j = 0; j < r; j++ )
        {
            float2 a = A[j];
            float2 b = B[(r - j + i) % r];

            c.x += a.x * b.x - a.y * b.y;
            c.y += a.x * b.y + a.y * b.x;           
        }
        C[i] = c;
    }
}

void poly_mul_fft( const float2* A, const float2* B, float2* C, int r )
{
    float2 _A[r];
    float2 _B[r];

    for( int i = 0; i < r; i++ )
    {
        _A[i] = A[i];
        _B[i] = B[i];
    }

    fft( _A, r );
    fft( _B, r );

    for( int i = 0; i < r; i++ )
    {
        C[i].x = _A[i].x * _B[i].x - _A[i].y * _B[i].y;
        C[i].y = _A[i].x * _B[i].y + _A[i].y * _B[i].x;         
    }

    ifft( C, r );
}

void fft( float2* X, int n )
{
    if( n <= 1 )
        return;

    float2 even[n/2];
    float2 odd[n/2];

    for (int i = 0; i < n/2; i++) {
        even[i] = X[2*i];
        odd[i] = X[2*i+1];
    }

    fft(even, n/2);
    fft(odd, n/2);

    for (int i = 0; i < n/2; i++) {
        float alpha = 2.0 * M_PI * i / n;        

        float2 w; // exp( - 2 * PI * I * i / n )
        w.x = cosf( alpha );
        w.y = -sinf( alpha );     

        float2 t;
        t.x = w.x * odd[i].x - w.y * odd[i].y;
        t.y = w.x * odd[i].y + w.y * odd[i].x;

        X[i].x = even[i].x + t.x;
        X[i].y = even[i].y + t.y;

        X[i + n/2].x = even[i].x - t.x;
        X[i + n/2].y = even[i].y - t.y;
    }
}

void ifft( float2* X, int n )
{
    if( n <= 1 )
        return;

    float2 even[n/2];
    float2 odd[n/2];

    for (int i = 0; i < n/2; i++) {
        even[i] = X[2*i];
        odd[i] = X[2*i+1];
    }

    ifft(even, n/2);
    ifft(odd, n/2);

    for (int i = 0; i < n/2; i++) {
        float alpha = 2.0 * M_PI * i / n;        

        float2 w; // exp( + 2 * PI * I * i / n )
        w.x = cosf( alpha );
        w.y = sinf( alpha );     

        float2 t;
        t.x = w.x * odd[i].x - w.y * odd[i].y;
        t.y = w.x * odd[i].y + w.y * odd[i].x;

        X[i].x = (even[i].x + t.x)/2.0;
        X[i].y = (even[i].y + t.y)/2.0;

        X[i + n/2].x = (even[i].x - t.x)/2.0;
        X[i + n/2].y = (even[i].y - t.y)/2.0;
    }
}



