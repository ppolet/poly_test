#!/usr/bin/env sh

Nl="32 64 128 256 512 1024 2048 4096"
Rl="32 64 128 256 512 1024 2048 4096"

for r in $Rl; do
    for n in $Nl; do
        ./build/gpu_test -m $n -n $n -k $n -r $r -l 32 -c 1
    done
done
