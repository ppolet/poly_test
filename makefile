CCOMP ?= gcc
EFLAGS ?= -Werror -Wall -Wpedantic

BUILD ?= build

ifdef CUDA_PATH
CUDA_CMD ?= -I $(CUDA_PATH)/include -L$(CUDA_PATH)/lib64
endif

all: $(BUILD)/add_test $(BUILD)/mul_test $(BUILD)/fft_test $(BUILD)/gpu_test $(BUILD)/gpu_validation

$(BUILD)/add_test: test.c | $(BUILD)
	$(CCOMP) $(EFLAGS) -o $@ test.c -DOPERATION=poly_add -lm

$(BUILD)/mul_test: test.c | $(BUILD)
	$(CCOMP) $(EFLAGS) -o $@ test.c -DOPERATION=poly_mul -lm

$(BUILD)/fft_test: test.c | $(BUILD)
	$(CCOMP) $(EFLAGS) -o $@ test.c -DOPERATION=poly_mul_fft -lm

$(BUILD)/gpu_test: gpu_test.c | $(BUILD)
	$(CCOMP) $(EFLAGS) -o $@ gpu_test.c $(CUDA_CMD) -lcudart -lcufft -lcublas

$(BUILD)/gpu_validation: gpu_test_validation.c | $(BUILD)
	$(CCOMP) $(EFLAGS) -o $@ gpu_test_validation.c $(CUDA_CMD) -lcudart -lcufft -lcublas

$(BUILD):
	mkdir -p $(BUILD)

clear:
	rm -rf $(BUILD)
